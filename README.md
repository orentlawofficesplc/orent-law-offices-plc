Orent Law Offices, PLC is one of the top criminal defense firms in Phoenix, AZ. Craig Orent has been practicing law for close to 30 years and is regarded by his peers as one of the best criminal lawyers in Arizona. With experience handling thousands of cases, Craig has helped people facing all kinds of criminal charges, including domestic violence, sex crimes, drug crimes, and more. Contact us today for a free consultation. 

Orent Law Offices, PLC

11811 N Tatum Blvd #3031, 
Phoenix, AZ 85028

(480) 656-7301

Website: https://www.orentcriminallaw.com
